package com.github.vitineth.node.mapper;

import com.github.vitineth.node.mapper.utils.DBIPLocationUtils;
import com.github.vitineth.node.mapper.utils.LocationUtils;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * Class Description
 * <p>
 * File created by Ryan (vitineth).<br>
 * Created on 01/04/2018.
 *
 * @author Ryan (vitineth)
 * @since 01/04/2018
 */
public class Traceroute {

    public static void main(String[] args) throws IOException, InterruptedException {
        // Load up the GeoIP database.
        // This takes a long time to do
        LocationUtils utils = new DBIPLocationUtils();// new GeoIpLocationUtils();

        if (!System.getProperty("os.name").toLowerCase().contains("win") && !(System.getProperty("os.name").contains("nix") || System.getProperty("os.name").contains("nux") || System.getProperty("os.name").contains("aix"))){
            System.err.println("Unknown OS");
            return;
        }

        boolean isLinux = System.getProperty("os.name").contains("nix") || System.getProperty("os.name").contains("nux") || System.getProperty("os.name").contains("aix");

        // Define the URLs that we want to lookup. Best to do more than one because of how long it takes to load the DB
        String[] urls = args;//new String[]{"google.co.uk", "bris.ac.uk", "bbc.com", "nasa.gov"};

        // Open a new runtime instance so we can execute traceroutes
        Runtime rt = Runtime.getRuntime();

        for (String url : urls) {
            // Output the context
            System.out.println("=== Executing Traceroute Lookup for [" + url + "] ===");

            // Construct and execute the command
            String[] commands = new String[]{isLinux ? "traceroute" : "tracert", url};
            Process process = rt.exec(commands);

            // Wait for the process to end so we can fetch the entire response from the console. We could do this in
            // real time but that seemed like too much work
            System.out.println("Beginning traceroute and waiting for response");
            process.waitFor();
//
//            // Read in everything from the console so we get the full response
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            byte[] buffer = new byte[1024];
            int read;
            while ((read = process.getInputStream().read(buffer)) != -1) baos.write(buffer, 0, read);
            String route = baos.toString();

            // Print out the real response. I decided to indent it by one tab just for ease of reading which is the
            // horrible line below. It splits to each line, appends a new line to each line then collects it all
            // together again with new lines.
            System.out.println("Raw response:");
            System.out.println(Arrays.stream(route.split("\n")).map(s -> "\t" + s).collect(Collectors.joining("\n")));

            // Output a divider ready for our own output
            System.out.println("\n=====\n");

            // Divide up the console output into its line
            String[] routeLines = route.split("\n");
            // This is a horrible regex. Basically is matches a valid line from the console. It will ignore lines that
            // do not conform the output 5    16 ms    12 ms    13 ms  ae23.londhx-sbr1.ja.net [146.97.35.165] (the ip
            // can be IPv6 as well). It will natively ignore times out exceptions.
            Pattern pattern = Pattern.compile("^(?:\\s+?([0-9]+?)\\s+?([0-9]+?\\s+?ms)\\s+?([0-9]+?\\s+?ms)\\s+?([0-9]+?\\s+?ms)\\s+?([0-9a-z-.:]+)(\\s+?)?(\\[([0-9.]+)](\\s+)?)?.*)|(?:(\\s)?([0-9]+?)(\\s+?)([a-zA-Z0-9.-]+?)(\\s+?)\\(([0-9.a-z:]+?)\\)\\s+?[0-9]+?\\s+?(ms\\s+?!|ms)\\s+?[0-9]+?\\s+?(ms\\s+?!|ms)\\s+?[0-9]+?\\s+?(ms\\s+?!|ms)?.*)$", Pattern.DOTALL);

            // We want to form a list of every valid hop in the text
            List<InetAddress> path = new ArrayList<>();
            // For each line in the traceroute
            for (String s : routeLines) {
                // We attempt to match the pattern and fetch a matcher instance
                Matcher matcher = pattern.matcher(s);
                matcher.matches();
                try {
                    // Then we attempt to fetch the hostname. If it fails because it does not match, it will throw an
                    // IllegalStateException which is ignored below. I know there are better ways but this was rushed
                    if (matcher.group(5) == null) path.add(InetAddress.getByName(matcher.group(13)));
                    else path.add(InetAddress.getByName(matcher.group(5)));
                } catch (IllegalStateException ignored) {
                } catch (UnknownHostException e) {
                    try {
                        // If we can't determine the host then we attempt to grab the IP address and resolve it. If
                        // this fails then we will just ignore that element
                        if (matcher.group(8) != null || matcher.group(15) != null) {
                            if (matcher.group(8) != null) path.add(InetAddress.getByName(matcher.group(8)));
                            else path.add(InetAddress.getByName(matcher.group(15)));
                        }
                    } catch (IllegalStateException ignored) {
                    }
                }
            }

            // For each hop in the path
            for (int i = 0; i < path.size(); i++) {
                // Output the hop
                System.out.println("Hop " + (i + 1) + ": " + path.get(i).toString());

                // Resolve the location and dump out the output
                String response = utils.getLocation(path.get(i));
                if (response != null) {
                    System.out.println("Server location (est): " + response);
                } else {
                    System.out.println("Server location (est): Could not be determined");
                }
            }
        }
    }

}
