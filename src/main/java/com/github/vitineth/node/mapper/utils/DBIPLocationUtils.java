package com.github.vitineth.node.mapper.utils;

import in.ankushs.dbip.api.DbIpClient;
import in.ankushs.dbip.api.GeoEntity;

import java.io.*;
import java.net.InetAddress;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Class Description
 * <p>
 * File created by Ryan (vitineth).<br>
 * Created on 16/04/2018.
 *
 * @author Ryan (vitineth)
 * @since 16/04/2018
 */
public class DBIPLocationUtils implements LocationUtils {

    private DbIpClient client;

    public DBIPLocationUtils() {
        client = new DbIpClient(new File("dbip-city-2018-04.csv.gz"));
    }

    @Override
    public String getLocation(InetAddress address) {
        GeoEntity entity = client.lookup(address);
        if (entity == null) return null;
        return entity.getCity() + ", " + entity.getCountry() + ", " + entity.getProvince() + ", " + entity.getCountryCode();
    }
}
