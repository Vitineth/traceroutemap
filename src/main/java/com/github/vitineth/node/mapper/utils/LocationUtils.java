package com.github.vitineth.node.mapper.utils;

import java.net.InetAddress;

/**
 * Class Description
 * <p>
 * File created by Ryan (vitineth).<br>
 * Created on 16/04/2018.
 *
 * @author Ryan (vitineth)
 * @since 16/04/2018
 */
public interface LocationUtils {

    String getLocation(InetAddress address);

}
